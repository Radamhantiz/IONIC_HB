import { Component } from '@angular/core';

import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs2.html'
})
export class Tabs2Page {

  tab1Root = HomePage;

  constructor() {

  }
}